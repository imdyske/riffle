package sk.limesoft.riffle

import android.widget.Toast
import org.jetbrains.anko.design.snackbar
import org.jsoup.Jsoup
import sk.limesoft.riffle.persistence.CardLightweightContentProvider

object CardManager {

    fun applyTransformations(card: Card): Card {
        var modCard = card
        card.transformations.forEach { modCard = it.transform(modCard) }
        return modCard
    }

    fun update(card: Card): Card {
        var jsonCorrection = false
        var errors = ""
        val content: String? =
                try {
                    if (card.json)
                        Jsoup.connect(card.url).ignoreContentType(true).execute().body()
                    else
                        try {
                            Jsoup.connect(card.url).get().toString()
                        } catch (ex: org.jsoup.UnsupportedMimeTypeException) {
                            if (ex.mimeType == "application/javascript" || ex.mimeType == "application/json") {
                                jsonCorrection = true
                                Jsoup.connect(card.url).ignoreContentType(true).execute().body()
                            } else card.raw
                        }
                } catch (ex: org.jsoup.HttpStatusException) {
                    errors = "Error: ${ex.statusCode}"
                    card.raw
                }

        if (!errors.isBlank())
            Toast.makeText(RiffleApplication.appContext, "Error: $errors", Toast.LENGTH_SHORT).show()

        val newCard = if (jsonCorrection)
            Card(card.url, Card.baseMap(card).plus("raw" to content).plus("json" to true))
        else Card(card.url, Card.baseMap(card).plus("raw" to content))

        val cardProvider = CardLightweightContentProvider()
        cardProvider.update(newCard)

        return  newCard
    }
}