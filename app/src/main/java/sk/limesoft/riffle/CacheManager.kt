package sk.limesoft.riffle

import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.widget.ImageView
import android.widget.Toast
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import java.net.HttpURLConnection
import java.net.MalformedURLException


object CacheManager {
    //private val cache = mutableMapOf<String, Drawable>()

    fun getDrawable(image: String, view: ImageView): Drawable? {
        //if (cache.containsKey(image)) return cache[image]

        doAsync {
            try {
                val url = java.net.URL(image)
                val connection = url.openConnection() as HttpURLConnection
                connection.useCaches = true
                connection.doInput = true
                connection.connect()
                val input = connection.inputStream
                val output = BitmapDrawable(RiffleApplication.appContext.resources, BitmapFactory.decodeStream(input))
                uiThread {
                    //cache[image] = output
                    view.setImageDrawable(output)
                }
            } catch (ex: MalformedURLException) {
                uiThread { Toast.makeText(RiffleApplication.appContext, "Malformed image url: $image", Toast.LENGTH_SHORT).show() }
            }
        }

        return null
    }
}
