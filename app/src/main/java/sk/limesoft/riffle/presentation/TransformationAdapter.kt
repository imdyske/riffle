package sk.limesoft.riffle.presentation

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageButton
import android.widget.TextView
import org.jetbrains.anko.sdk25.coroutines.onClick
import sk.limesoft.riffle.R
import sk.limesoft.riffle.Transformation
import sk.limesoft.riffle.persistence.TransformationLightweightContentProvider

class TransformationAdapter(context: Context, private var transformationList: MutableList<Transformation>) : BaseAdapter() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    class ViewHolder(itemView: View) {
        var nameTextView: TextView = itemView.findViewById(R.id.transformationNameTextView) as TextView
        var parameterTextView: TextView = itemView.findViewById(R.id.transformationParameterTextView) as TextView
        var removeButton = itemView.findViewById(R.id.transformationRemoveButton) as ImageButton
    }
    fun refresh(items: MutableList<Transformation>) { transformationList = items; notifyDataSetChanged() }
    override fun getItem(pos: Int): Transformation = transformationList[pos]
    override fun getItemId(p0: Int): Long = 0
    override fun getCount(): Int = transformationList.size
    override fun getView(pos: Int, cView: View?, parent: ViewGroup?): View? {
        val holder: ViewHolder
        var view: View? = null
        if (cView == null) {
            view = inflater.inflate(R.layout.transformation_list_item, parent, false)
            holder = ViewHolder(view)
            view.tag = holder
        } else holder = cView.tag as ViewHolder

        val currentTransformation: Transformation = getItem(pos)
        with(holder) {
            nameTextView.text = """${currentTransformation.name} on ${currentTransformation.key}"""
            parameterTextView.text = currentTransformation.parameter
            removeButton.onClick {
                val transformationProvider = TransformationLightweightContentProvider(-1)
                transformationProvider.delete(currentTransformation)

                transformationList.remove(currentTransformation)
                notifyDataSetChanged()
            }
        }
        return cView ?: view
    }
}

