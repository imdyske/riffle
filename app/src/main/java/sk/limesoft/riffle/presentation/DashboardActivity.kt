package sk.limesoft.riffle.presentation

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.app_bar_dashboard.*
import kotlinx.android.synthetic.main.content_dashboard.*
import org.jetbrains.anko.design.snackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.support.v4.onRefresh
import org.jetbrains.anko.uiThread
import sk.limesoft.riffle.Card
import sk.limesoft.riffle.CardManager
import sk.limesoft.riffle.R
import sk.limesoft.riffle.RiffleApplication
import sk.limesoft.riffle.persistence.CardLightweightContentProvider
import sk.limesoft.riffle.persistence.LightweightContentProvider


class DashboardActivity : AppCompatActivity()
{
    private lateinit var cardList: MutableList<Card>
    private lateinit var cardProvider: LightweightContentProvider<Card>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        setSupportActionBar(toolbar)

        cardProvider = CardLightweightContentProvider()
        cardList = cardProvider.queryAll().toMutableList()

        fab.visibility = View.GONE
        fab.setOnClickListener { _ ->
            val intent = Intent(this, BasicCardActivity::class.java)
            startActivity(intent)
        }

        rView.adapter = CardAdapter(this, cardList)
        val spanCount = when (resources.configuration.orientation) {
            Configuration.ORIENTATION_LANDSCAPE -> 2
            Configuration.ORIENTATION_PORTRAIT -> 1
            else -> 1
        }
        rView.layoutManager = GridLayoutManager(this, spanCount, GridLayoutManager.VERTICAL, false)

        swipeRefreshLayout.onRefresh {
            if (!isNetworkAvailable()) {
                snackbar(swipeRefreshLayout, "No network connection")
                swipeRefreshLayout.isRefreshing = false
            } else {
                doAsync {
                    // todo err
                    for (i in cardList.indices) {
                        val temp = CardManager.update(cardList[i])
                        uiThread {
                            it.cardList[i] = CardManager.applyTransformations(temp)
                            rView.adapter.notifyItemChanged(i)
                        }
                    }
                    uiThread { swipeRefreshLayout.isRefreshing = false }
                }
            }
        }
    }

    private fun isNetworkAvailable(): Boolean {
        val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.dashboard, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            R.id.action_add_card -> { showNewCard(); true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun showNewCard() {
        val intent = Intent(this, CardSettingsActivity::class.java).apply {
            putExtra(RiffleApplication.CARD, Card("", Card.emptyMap))
        }
        startActivityForResult(intent, 1)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1)
            when (resultCode) {
                RESULT_OK -> {
                    var card = data?.getSerializableExtra(RiffleApplication.CARD) as Card
                    var position = data.getSerializableExtra(RiffleApplication.POSITION) as Int?
                    if (position != null) {
                        cardList[position] = card
                        rView.adapter.notifyItemChanged(position)
                    } else {
                        card = cardProvider.insert(card)
                        cardList.add(card)
                        position = cardList.size - 1
                        rView.adapter.notifyItemInserted(position)
                    }
                    card = CardManager.applyTransformations(card)
                    doAsync {
                        card = CardManager.update(card)
                        uiThread {
                            it.cardList[position] = CardManager.applyTransformations(card)
                            rView.adapter.notifyItemChanged(position)
                        }
                    }
                }
                RESULT_CANCELED -> {}
            }
    }
}

