package sk.limesoft.riffle.presentation

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.widget.ListView

class DynamicHeightListView(context: Context, attrs: AttributeSet) : ListView(context, attrs) {
    private var params: android.view.ViewGroup.LayoutParams? = null
    private var oldCount = 0

    override fun onDraw(canvas: Canvas) {
        if (count != oldCount) {
            val height = getChildAt(0)?.height?.plus(1)
            oldCount = count
            params = layoutParams
            params!!.height = count * (height ?: 1)
            layoutParams = params
        }

        super.onDraw(canvas)
    }
}