package sk.limesoft.riffle.presentation

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.os.Bundle
import android.widget.EditText
import android.widget.Spinner
import sk.limesoft.riffle.R
import sk.limesoft.riffle.RiffleApplication
import sk.limesoft.riffle.Transformation
import sk.limesoft.riffle.setToValue


class TransformationDialogFragment : DialogFragment() {

    interface OnTransformationEditListener { fun onTransformationEdit(transformation: Transformation, position: Int) }

    private var position: Int = 0
    private lateinit var transformation: Transformation
    private lateinit var callback: OnTransformationEditListener

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        if (context is Activity)
            callback = context as OnTransformationEditListener
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            transformation = it.get(RiffleApplication.TRANSFORMATION) as Transformation
            position = it.getInt(RiffleApplication.POSITION)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity)

        val inflater = activity.layoutInflater
        val view = inflater.inflate(R.layout.fragment_transformation_dialog, null)
        val typeSpinner = view.findViewById<Spinner>(R.id.transformationTypeSpinner)
        val keySpinner = view.findViewById<Spinner>(R.id.transformationKeySpinner)
        val parameterEditText = view.findViewById<EditText>(R.id.transformationParameterEditText)
        typeSpinner.setToValue(transformation.name)
        keySpinner.setToValue(transformation.key)
        parameterEditText.setText(transformation.parameter)

        builder.setView(view)
        builder.setPositiveButton("Ok", { _, _ ->
            val key = keySpinner.selectedItem as String
            val parameter = parameterEditText.text.toString()
            val newTransformation: Transformation = Transformation.create(typeSpinner.selectedItem as String, parameter, key)
            newTransformation.id = transformation.id
            callback.onTransformationEdit(newTransformation, position)
        })
        builder.setNegativeButton("Cancel", { _, _ -> dismiss() })
        return builder.create()
    }
}

