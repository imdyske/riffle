package sk.limesoft.riffle.presentation

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.support.design.widget.Snackbar
import android.support.v7.widget.CardView
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.design.longSnackbar
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.sdk25.coroutines.onLongClick
import org.jetbrains.anko.uiThread
import sk.limesoft.riffle.*
import sk.limesoft.riffle.persistence.CardLightweightContentProvider


class CardAdapter(private val context : Context, private val list : MutableList<Card>) : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    class SimpleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var primaryTextView: TextView = itemView.findViewById(R.id.primary) as TextView
        var secondaryTextView: TextView = itemView.findViewById(R.id.secondary) as TextView
        val cardView: CardView = itemView.findViewById(R.id.card_full_simple) as CardView
    }

    class TitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var primaryTextView: TextView = itemView.findViewById(R.id.primary) as TextView
        var secondaryTextView: TextView = itemView.findViewById(R.id.secondary) as TextView
        var imageView: ImageView = itemView.findViewById(R.id.imageView) as ImageView
        val cardView: CardView = itemView.findViewById(R.id.card_full_title) as CardView
    }

    class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var primaryTextView: TextView = itemView.findViewById(R.id.primary) as TextView
        var imageView: ImageView = itemView.findViewById(R.id.imageView) as ImageView
        val cardView: CardView = itemView.findViewById(R.id.card_full_image) as CardView
    }

    override fun getItemViewType(position: Int): Int {
        return list[position].layout
    }

    override fun onCreateViewHolder(parent : ViewGroup, type : Int) : RecyclerView.ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.card_full_simple, parent, false)
        when (type) {
            0 -> {
                val view : View = LayoutInflater.from(parent.context).inflate(R.layout.card_full_simple, parent, false)
                return SimpleViewHolder(view)
            }
            1 -> {
                val view : View = LayoutInflater.from(parent.context).inflate(R.layout.card_full_image, parent, false)
                return ImageViewHolder(view)
            }
            2 -> {
                val view : View = LayoutInflater.from(parent.context).inflate(R.layout.card_full_title, parent, false)
                return TitleViewHolder(view)
            }
        }
        return SimpleViewHolder(view)
    }

    override fun onBindViewHolder(holder : RecyclerView.ViewHolder, position : Int){
        val card : Card = list[position]
        when (getItemViewType(position)) {
            0 -> { val vh = holder as SimpleViewHolder
                vh.cardView.setCardBackgroundColor(card.color)
                vh.cardView.onLongClick { showPopupMenu(vh.cardView, card) }
                vh.primaryTextView.text = card.primary
                vh.primaryTextView.setTextColor(card.textColor)
                vh.secondaryTextView.text = card.secondary
                vh.secondaryTextView.setTextColor(card.textColor)
            }
            1 -> {
                val vh = holder as ImageViewHolder
                vh.cardView.setCardBackgroundColor(card.color)
                vh.cardView.onLongClick { showPopupMenu(vh.cardView, card) }
                vh.primaryTextView.text = card.primary
                vh.primaryTextView.setTextColor(card.textColor)
                vh.imageView.setImageDrawable(CacheManager.getDrawable(card.image, vh.imageView))
            }
            2 -> {
                val vh = holder as TitleViewHolder
                vh.cardView.setCardBackgroundColor(card.color)
                vh.cardView.onLongClick { showPopupMenu(vh.cardView, card) }
                vh.primaryTextView.text = card.primary
                vh.primaryTextView.setTextColor(card.textColor)
                vh.secondaryTextView.text = card.secondary
                vh.secondaryTextView.setTextColor(card.textColor)
                vh.imageView.setImageDrawable(CacheManager.getDrawable(card.image, vh.imageView))
            }
        }
    }

    private fun editCard(card: Card) {
        val intent = Intent(context, CardSettingsActivity::class.java).apply {
            putExtra(RiffleApplication.CARD, card)
            putExtra(RiffleApplication.TITLE, "Edit a card")
            putExtra(RiffleApplication.POSITION, list.indexOf(card))
        }
        (context as Activity).startActivityForResult(intent, 1)
    }

    private fun showPopupMenu(view: View, card: Card) {
        val popup = PopupMenu(context, view)
        val inflater = popup.menuInflater
        inflater.inflate(R.menu.card_popup_menu, popup.menu)
        popup.setOnMenuItemClickListener {
            val idx = list.indexOf(card)
            when (it.itemId) {
                R.id.card_edit -> { editCard(card); true }
                R.id.card_refresh -> {
                    doAsync {
                        val tmp = CardManager.update(card)
                        uiThread {
                            list[idx] = tmp
                            notifyItemChanged(idx)
                        }
                    }
                    true
                }
                R.id.card_delete -> {
                    list.remove(card)
                    notifyItemRemoved(idx)
                    longSnackbar(view, "Card removed", "Undo",
                             { _: View -> list.add(idx, card); notifyItemInserted(idx) })
                            .addCallback(object: Snackbar.Callback() {
                                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                                    super.onDismissed(transientBottomBar, event)
                                    val cardProvider = CardLightweightContentProvider()
                                    cardProvider.delete(card)
                                }
                            }).show()
                    true
                }
                else -> { false }
            }
        }
        popup.show()
    }

    override fun getItemCount() : Int{
        return list.size
    }
}
