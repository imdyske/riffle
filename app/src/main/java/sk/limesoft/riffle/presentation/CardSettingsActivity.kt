package sk.limesoft.riffle.presentation

import android.content.Intent
import android.content.res.ColorStateList
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.CardView
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewTreeObserver.OnPreDrawListener
import android.widget.TextView
import com.android.colorpicker.ColorPickerDialog
import kotlinx.android.synthetic.main.content_card_settings.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.sdk25.coroutines.onItemClick
import org.jetbrains.anko.sdk25.coroutines.onItemSelectedListener
import com.jayway.jsonpath.internal.JsonFormatter
import org.jetbrains.anko.*
import org.jsoup.Jsoup
import sk.limesoft.riffle.*


@Suppress("UsePropertyAccessSyntax")
class CardSettingsActivity : AppCompatActivity(), TransformationDialogFragment.OnTransformationEditListener {

    companion object {
        fun runJustBeforeBeingDrawn(view: View, runnable: Runnable) {
            val preDrawListener = object : OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    view.viewTreeObserver.removeOnPreDrawListener(this)
                    runnable.run()
                    return true
                }
            }
            view.viewTreeObserver.addOnPreDrawListener(preDrawListener)
        }
    }

    private lateinit var card: Card
    private var iRaw: String? = null
        set(value) {
            field = if (iRaw == null) value else iRaw}
    private lateinit var transformations: MutableList<Transformation>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_settings)
        setSupportActionBar(toolbar)

        val title = intent.getSerializableExtra(RiffleApplication.TITLE) as String?
        if (title != null)
            setTitle(title)

        if (savedInstanceState == null)
            importCard(intent.getSerializableExtra(RiffleApplication.CARD) as Card)
        else
            importCard(savedInstanceState.getSerializable(RiffleApplication.CARD) as Card)

        iRaw = card.raw

        val cards = listOf(R.layout.card_full_simple, R.layout.card_full_image, R.layout.card_full_title)
        val heights = mutableListOf(-1, -1, -1)

        val inflater = LayoutInflater.from(cardViewFlipper.context)
        for (i in cards.indices) {
            val view = inflater.inflate(cards[i], null, false)
            view.childrenSequence().first().tag = "card_preview"
            cardViewFlipper.addView(view)
            runJustBeforeBeingDrawn(view, Runnable {
                run { heights[i] = view.childrenSequence().first().measuredHeight }
            })
        }

        fun toggleVisibility(v: View) { v.visibility = if (v.visibility == View.VISIBLE) View.GONE else View.VISIBLE }
        bt_toggle_items.setOnClickListener { toggleVisibility(styleLinearLayout) }
        bt_toggle_address.setOnClickListener { toggleVisibility(previewLinearLayout) }
        bt_toggle_description.setOnClickListener { toggleVisibility(transformationsLinearLayout) }
        forceSSLCheckbox.setOnCheckedChangeListener { _, state ->
            val from = if (state) "http://" else "https://"
            val to = if (state) "https://" else "http://"
            val changed = cardUrlText.text.replaceFirst(Regex.fromLiteral(from), to)
            cardUrlText.setText(changed, TextView.BufferType.EDITABLE)
        }
        cardTypeSpinner.onItemSelectedListener {
            onItemSelected { _, _, pos, _ ->
                cardViewFlipper.displayedChild = pos
                cardViewFlipper.layoutParams.height = heights[pos] + dip(36)
                cardViewFlipper.requestLayout()
                importCard(exportCard(), true)
            }
        }
        fun showColorPicker(colors: IntArray, callback: (Int) -> Unit) {
            val cpd = ColorPickerDialog.newInstance(R.string.choose_a_color, colors, 0, 4, 2)
            cpd.setOnColorSelectedListener { callback(it) }
            cpd.show(fragmentManager, "colorpicker")
        }
        val cardColors = resources.getIntArray(R.array.cardColors)
        val textColors = resources.getIntArray(R.array.textColors)
        cardColorTextView.setOnClickListener { showColorPicker(cardColors, ::cardColor) }
        cardColorButton.setOnClickListener { showColorPicker(cardColors, ::cardColor) }
        textColorTextView.setOnClickListener { showColorPicker(textColors, ::textColor) }
        textColorButton.setOnClickListener { showColorPicker(textColors, ::textColor) }

        addTransformationButton.onClick { showTransformationDialog(Transformation("", "primary"), -1)  }
        with (transformationListView) {
            adapter = TransformationAdapter(ctx, transformations)
            itemsCanFocus = true
            isFocusableInTouchMode = false
            isClickable = false
            onItemClick { adapter, _, pos, _ ->
                val transf = adapter?.getItemAtPosition(pos) as Transformation
                showTransformationDialog(transf, pos)
            }
        }
        previewRefreshButton.onClick {
            if (iRaw == null || iRaw!!.isEmpty()) {
                doAsync { val tmp = CardManager.update(exportCard()); uiThread { iRaw = tmp.raw } }
            }
            importCard(CardManager.applyTransformations(Card(card.url, Card.baseMap(exportCard())
                    + ("primary" to "") + ("secondary" to "") + ("image" to "") + ("raw" to iRaw))))
            cardRaw(iRaw as String)
        }
        rawTextView.isSelected = true
        rawTextView.setHorizontallyScrolling(true)
        cardViewFlipper.post { importCard(CardManager.applyTransformations(exportCard())) }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.card_settings, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_save_card -> { saveCard(); true }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun saveCard() {
        val output = Intent()
        val position = intent.getSerializableExtra(RiffleApplication.POSITION) as Int?
        output.putExtra(RiffleApplication.CARD, exportCard())
        output.putExtra(RiffleApplication.POSITION, position)
        setResult(RESULT_OK, output)
        finish()
    }

    private fun getPreview() = cardViewFlipper?.getChildAt(cardViewFlipper.displayedChild)?.findViewWithTag<CardView>("card_preview")
    private fun cardPrimary(s: String) = getPreview()?.findViewById<TextView>(R.id.primary)?.setText(if (s.isEmpty()) "<primary>" else s)
    private fun cardSecondary(s: String) = getPreview()?.findViewById<TextView>(R.id.secondary)?.setText(if (s.isEmpty()) "<secondary>" else s)
    private fun cardRaw(s: String) = rawTextView.setText(if (card.json) JsonFormatter.prettyPrint(s) else Jsoup.parse(s).body().html())
    private fun cardUrl(url: String) = cardUrlText.setText(url)
    private fun cardSSL(url: String) { forceSSLCheckbox.isChecked = (url.startsWith("https://")) }
    private fun cardJSON(isJSON: Boolean) { isJSONCheckbox.isChecked = isJSON }
    private fun cardLayout(pos: Int) { cardTypeSpinner.setSelection(pos) }
    private fun textColor(color: Int) {
        textColorButton.imageTintList = ColorStateList.valueOf(color)
        getPreview()?.findViewById<TextView>(R.id.primary)?.setTextColor(color)
        getPreview()?.findViewById<TextView>(R.id.secondary)?.setTextColor(color)
    }
    private fun cardColor(color: Int) {
        cardColorButton.imageTintList = ColorStateList.valueOf(color)
        getPreview()?.setCardBackgroundColor(color)
    }
    private fun cardTransformations(transformations: MutableList<Transformation>?) {
        if (transformations != null || transformations?.size != 0) {
            noTransformationsTextView.visibility = View.GONE
            transformationListView.visibility = View.VISIBLE
            this.transformations = transformations!!
            (transformationListView.adapter as TransformationAdapter?)?.refresh(this.transformations)
        } else {
            noTransformationsTextView.visibility = View.VISIBLE
            transformationListView.visibility = View.GONE
        }
    }

    override fun onTransformationEdit(transformation: Transformation, position: Int) {
        when (position) {
            -1 -> { transformations.add(transformation)  }
            else -> { transformations[position] = transformation }
        }
        cardTransformations(transformations)
    }
    private fun showTransformationDialog(t: Transformation, p: Int) {
        val fragment = TransformationDialogFragment()
        fragment.arguments = Bundle().apply {
            putSerializable(RiffleApplication.TRANSFORMATION, t)
            putInt(RiffleApplication.POSITION, p)
        }
        fragment.show(fragmentManager, "Transformation Fragment")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(RiffleApplication.CARD, exportCard())
    }

    private fun importCard(c: Card, skipLayout: Boolean = false) {
        card = c
        if (!skipLayout) cardLayout(c.layout)
        cardColor(c.color)
        textColor(c.textColor)
        cardUrl(c.url)
        cardSSL(c.url)
        cardTransformations(c.transformations.toMutableList())
        cardJSON(c.json)
        cardPrimary(c.primary)
        cardSecondary(c.secondary)
        cardRaw(c.raw)
    }

    private fun exportCard(): Card {
        return Card(cardUrlText.text.toString(), Card.baseMap(card) + mapOf(
                "color" to cardColorButton.imageTintList.defaultColor,
                "textColor" to textColorButton.imageTintList.defaultColor,
                "json" to isJSONCheckbox.isChecked,
                "transformations" to transformations,
                "layout" to cardTypeSpinner.selectedItemPosition
        ))
    }
}
