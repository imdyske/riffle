package sk.limesoft.riffle

import android.app.Application
import android.content.Context


class RiffleApplication : Application() {


    override fun onCreate() {
        super.onCreate()
        RiffleApplication.appContext = applicationContext
    }

    companion object {
        lateinit var appContext: Context
            private set
        const val CARD: String = "sk.limesoft.riffle.CARD"
        const val POSITION: String = "sk.limesoft.riffle.POSITION"
        const val TITLE: String = "sk.limesoft.riffle.TITLE"
        const val TRANSFORMATION: String = "sk.limesoft.riffle.TRANSFORMATION"
    }
}