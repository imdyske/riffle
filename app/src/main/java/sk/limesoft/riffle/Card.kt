package sk.limesoft.riffle

import android.graphics.drawable.Drawable
import android.support.v4.content.res.ResourcesCompat
import org.jetbrains.anko.db.MapRowParser
import java.io.Serializable

class Card (val url: String, map: Map<String, Any?>) : Serializable {
    val raw: String by map
    val transformations : List<Transformation> by map
    val primary: String by map
    val secondary: String by map
    val image: String by map
    val layout: Int by map
    val color: Int by map
    val textColor: Int by map
    val json: Boolean by map
    var id: Long = -1
        set(value) { field = if (id == -1L) value else id }
    init { id = if (map.containsKey("id")) map["id"] as Long else -1 }

    companion object {
        var baseMap: (Card) -> MutableMap<String, Any?> = { c -> mutableMapOf(
                "id"              to c.id,
                "layout"          to c.layout,
                "raw"             to c.raw,
                "primary"         to c.primary,
                "secondary"       to c.secondary,
                "image"           to c.image,
                "color"           to c.color,
                "textColor"       to c.textColor,
                "json"            to c.json,
                "transformations" to c.transformations
        )}

        val emptyMap: Map<String, Any?> = mapOf(
                "layout"          to 0,
                "raw"             to "",
                "primary"         to "",
                "secondary"       to "",
                "image"           to "",
                "color"           to ResourcesCompat.getColor(RiffleApplication.appContext.resources, R.color.blue_A400, null),
                "textColor"       to ResourcesCompat.getColor(RiffleApplication.appContext.resources, R.color.grey_10, null),
                "json"            to false,
                "transformations" to emptyList<Transformation>()
        )
    }
}

class CardRowParser: MapRowParser<Card> {
    override fun parseRow(columns: Map<String, Any?>): Card {
        val cardMap = columns
                .minus("url")
                .plus("transformations" to Card.emptyMap["transformations"])
                .plus("json" to (columns["json"] == 1L))
        val card = Card(columns["url"] as String, cardMap)
        card.id = columns["_id"] as Long
        return card
    }
}

