package sk.limesoft.riffle

import org.jetbrains.anko.db.MapRowParser
import sk.limesoft.riffle.transformations.*
import java.io.Serializable

open class Transformation(val parameter: String, val key: String): Serializable {
    companion object {
        fun create(name: String, parameter: String, key: String) = when (name) {
            "Static text" -> StaticTextTransformation(parameter, key)
            "Prepend a string" -> PrependTransformation(parameter, key)
            "Append a string" -> AppendTransformation(parameter, key)
            "Regular expression match" -> RegexTransformation(parameter, key)
            "DOM content selection (css)" -> CSSSelectTransform(parameter, key)
            "DOM content selection (xpath)" -> XPathSelectTransform(parameter, key)
            "JSON content selection" -> JSONSelectTransformation(parameter, key)
            "Raw output" -> RawOutputTransformation(parameter, key)
            else -> Transformation(parameter, key)
        }
    }

    var id: Long = -1
        set(value) { field = if (id == -1L) value else id }
    open val name: String = "Blank"

    open fun transform(input: Card): Card {
        return Card(input.url, Card.baseMap(input).toMap())
    }
}

class TransformationRowParser: MapRowParser<Transformation> {
    override fun parseRow(columns: Map<String, Any?>): Transformation {
        val t = Transformation.create(columns["name"] as String, columns["parameter"] as String, columns["key"] as String)
        t.id = columns["_id"] as Long
        return t
    }
}

