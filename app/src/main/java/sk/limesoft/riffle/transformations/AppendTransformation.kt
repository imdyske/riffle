package sk.limesoft.riffle.transformations

import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class AppendTransformation(val p: String, k:String): Transformation(p, k) {
    override val name: String = "Append a string"

    override fun transform(input: Card): Card {
        val map = Card.baseMap(input)
        map[key] = map[key] as String + parameter
        return Card(input.url, map.toMap())
    }
}