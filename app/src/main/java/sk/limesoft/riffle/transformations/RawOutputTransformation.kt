package sk.limesoft.riffle.transformations

import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class RawOutputTransformation(val p: String, k:String): Transformation(p, k) {
    override val name: String = "Raw output"

    override fun transform(input: Card): Card {
        val map = Card.baseMap(input)
        map[key] = input.raw
        return Card(input.url, map.toMap())
    }
}