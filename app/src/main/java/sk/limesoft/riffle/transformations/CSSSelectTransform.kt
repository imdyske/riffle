package sk.limesoft.riffle.transformations

import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class CSSSelectTransform(val p: String, k:String): Transformation(p, k) {
    override val name: String = "DOM content selection (css)"

    override fun transform(input: Card): Card {
        val doc: Document = Jsoup.parse(input.raw as String?)
        val map = Card.baseMap(input)
        map[key] = doc.select(parameter).text()
        return Card(input.url, map.toMap())
    }
}

