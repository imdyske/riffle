package sk.limesoft.riffle.transformations

import com.jayway.jsonpath.Configuration
import com.jayway.jsonpath.JsonPath
import com.jayway.jsonpath.Option
import com.jayway.jsonpath.PathNotFoundException
import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class JSONSelectTransformation(val p: String, k:String): Transformation(p, k) {
    override val name: String = "JSON content selection"

    override fun transform(input: Card): Card {
        val conf = Configuration.defaultConfiguration().addOptions(Option.ALWAYS_RETURN_LIST)
        val doc = conf.jsonProvider().parse(input.raw)
        val map = Card.baseMap(input)
        val list = try {
            JsonPath.read<List<String>>(doc, parameter)
        } catch (e: Exception) {
            when (e) {
                is ClassCastException, is PathNotFoundException ->
                    JsonPath.read<String>(doc, parameter)
                else -> throw e
            }
        }
        map[key] = when (list) {
            is String -> { list }
            is List<*> -> { list.joinToString("\n") }
            else -> { "Error: neither string, nor array" }
        }
        return Card(input.url, map.toMap())
    }
}