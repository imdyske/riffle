package sk.limesoft.riffle.transformations

import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class PrependTransformation(val p: String, k:String): Transformation(p, k) {
    override val name: String = "Prepend a string"

    override fun transform(input: Card): Card {
        val map = Card.baseMap(input)
        map[key] = parameter + map[key] as String
        return Card(input.url, map.toMap())
    }
}