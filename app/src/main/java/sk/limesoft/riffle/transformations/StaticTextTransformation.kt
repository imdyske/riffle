package sk.limesoft.riffle.transformations

import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class StaticTextTransformation(val p: String, k:String): Transformation(p, k) {
    override val name: String = "Static text"

    override fun transform(input: Card): Card {
        val map = Card.baseMap(input)
        map[key] = parameter
        return Card(input.url, map.toMap())
    }
}