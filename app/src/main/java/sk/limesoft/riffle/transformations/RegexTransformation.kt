package sk.limesoft.riffle.transformations

import sk.limesoft.riffle.Card
import sk.limesoft.riffle.Transformation

class RegexTransformation(val p: String, k: String): Transformation(p, k) {
    override val name: String = "Regular expression match"

    override fun transform(input: Card) : Card {
        val map = Card.baseMap(input)
        val matches = Regex.fromLiteral(parameter).find(input.raw)?.groupValues
        val res = when (matches?.size) {
            0 -> "No matches"
            1 -> matches[0]
            else -> matches?.joinToString()
        } ?: "No matches"
        map[key] = res
        return Card(input.url, map.toMap())
    }
}