package sk.limesoft.riffle.persistence

import android.util.Log.d
import org.jetbrains.anko.db.*
import sk.limesoft.riffle.*

class CardLightweightContentProvider: LightweightContentProvider<Card> {
    private val context = RiffleApplication.appContext
    private val db: ManagedSQLiteOpenHelper = context.database

    override fun queryAll(): List<Card> {
        return db.use {
            val cards = select("Card").parseList(CardRowParser()).toMutableList()
            for (i in cards.indices) {
                val card = cards[i]
                val transformationProvider = TransformationLightweightContentProvider(card.id)
                val mod = card.transformations.toMutableList()
                mod.addAll(transformationProvider.queryAll())
                cards[i] = Card(card.url, Card.baseMap(card).plus("transformations" to mod))
                cards[i].id = card.id
                d("cards", cards.toString())
            }
            cards
        }
    }

    override fun insert(obj: Card): Card {
        db.use {
            obj.id = insert("Card",
                            "color" to obj.color, "json" to obj.json, "layout" to obj.layout,
                            "textColor" to obj.textColor, "image" to obj.image, "raw" to obj.raw,
                            "url" to obj.url, "`primary`" to obj.primary, "secondary" to obj.secondary)

            val transformationProvider = TransformationLightweightContentProvider(obj.id)
            obj.transformations.forEach { transformationProvider.insert(it) }
        }
        return obj
    }

    override fun update(obj: Card) {
        db.use {
            update("Card",
                   "color" to obj.color, "json" to obj.json,
                   "layout" to obj.layout, "textColor" to obj.textColor,
                   "image" to obj.image, "raw" to obj.raw,
                   "`primary`" to obj.primary, "secondary" to obj.secondary)
                    .whereSimple("_id = ?", obj.id.toString()).exec()

            val transformationProvider = TransformationLightweightContentProvider(obj.id)
            obj.transformations.forEach { transformationProvider.update(it) }
        }
    }

    override fun delete(obj: Card) {
        db.use {
            delete("Card", "_id = {id}", "id" to obj.id)
            val transformationProvider = TransformationLightweightContentProvider(obj.id)
            obj.transformations.forEach { transformationProvider.delete(it) }
        }
    }

    override fun delete(id: Long) {
        db.use {
            delete("Card", "_id = {id}", "id" to id)
            val transformationProvider = TransformationLightweightContentProvider(id)
            transformationProvider.queryAll().forEach { transformationProvider.delete(it) }
        }
    }
}