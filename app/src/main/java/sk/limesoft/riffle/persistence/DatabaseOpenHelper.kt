package sk.limesoft.riffle.persistence

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.util.Log.d
import org.jetbrains.anko.db.*

class DatabaseOpenHelper (val context: Context): ManagedSQLiteOpenHelper (context, "CardDatabase", null, 1) {

    companion object {
        private var instance: DatabaseOpenHelper? = null

        @Synchronized
        fun Instance(context: Context): DatabaseOpenHelper {
            if (instance == null) {
                instance = DatabaseOpenHelper(context.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.createTable("Card", true,
                       "_id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
                       "color" to INTEGER,
                       "json" to INTEGER,
                       "layout" to INTEGER,
                       "textColor" to INTEGER,
                       "url" to TEXT,
                       "image" to TEXT,
                       "raw" to TEXT,
                       "`primary`" to TEXT,
                       "secondary" to TEXT)
        db.createTable("Transformation", true,
                       "_id" to INTEGER + PRIMARY_KEY + AUTOINCREMENT + UNIQUE,
                       "card_id" to INTEGER,
                       "name" to TEXT,
                       "parameter" to TEXT,
                       "key" to TEXT,
                       FOREIGN_KEY("card_id", "Card", "_id"))
    }

    override fun onUpgrade(db: SQLiteDatabase, old: Int, new: Int) {
        d("DATABASE", "Would drop tables, now.")
        //db.dropTable("Card", true)
        //db.dropTable("Transformations", true)
    }
}
