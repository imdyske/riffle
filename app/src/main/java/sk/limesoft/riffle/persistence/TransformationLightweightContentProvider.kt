package sk.limesoft.riffle.persistence

import org.jetbrains.anko.db.*
import sk.limesoft.riffle.*

class TransformationLightweightContentProvider(private val card_id: Long): LightweightContentProvider<Transformation> {
    private val context = RiffleApplication.appContext
    private val db: ManagedSQLiteOpenHelper = context.database

    override fun queryAll(): List<Transformation> {
        return db.use {
            select("Transformation")
                    .whereArgs("card_id = {cid}", "cid" to card_id)
                    .parseList(TransformationRowParser())
        }
    }

    override fun insert(obj: Transformation): Transformation {
        obj.id = db.use { insert("Transformation", "card_id" to card_id,
                         "name" to obj.name, "parameter" to obj.parameter, "key" to obj.key) }
        return obj
    }

    override fun update(obj: Transformation) {
        db.use { update("Transformation", "card_id" to card_id,
                        "name" to obj.name, "parameter" to obj.parameter, "key" to obj.key)
                .whereSimple("_id = ?", obj.id.toString()).exec() }
    }

    override fun delete(obj: Transformation) {
        db.use { delete("Transformation", "_id = {id}", "id" to obj.id) }
    }

    override fun delete(id: Long) {
        db.use { delete("Transformation", "_id = {id}", "id" to id) }
    }
}

