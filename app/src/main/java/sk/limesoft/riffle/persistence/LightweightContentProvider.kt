package sk.limesoft.riffle.persistence

interface LightweightContentProvider<T> {
    fun queryAll(): List<T>
    fun insert(obj: T): T
    fun update(obj: T)
    fun delete(obj: T)
    fun delete(id: Long)
}