package sk.limesoft.riffle

import android.content.Context
import android.widget.Spinner
import sk.limesoft.riffle.persistence.DatabaseOpenHelper

fun Spinner.setToValue(value: String?) {
    for (i in 0 until this.count)
        if (this.getItemAtPosition(i) == value)
            this.setSelection(i)
}

val Context.database: DatabaseOpenHelper
    get() = DatabaseOpenHelper.Instance(applicationContext)

fun (() -> Unit).catch(vararg exceptions: Throwable, catchBlock: (Throwable) -> Unit) {
    try {
        this()
    } catch (e: Throwable) {
        if (e in exceptions) catchBlock(e) else throw e
    }
}

